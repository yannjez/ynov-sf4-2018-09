<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TestExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('money', [$this, 'money']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('money', [$this, 'money']),
        ];
    }

    public function money($value)
    {
        return number_format($value, 2,',',' ') . '€';
    }
}
