<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */

    public function findBySlug($slug)
    {
        $query =  $this->createQueryBuilder('a')
            ->andWhere('a.slug = :val')
            ->setParameter(':val', $slug)
            ->getQuery();

       // var_dump($query,$slug);
       // $query->execute($query->getParameters());
         $result  = $query->getOneOrNullResult()      ;
       // var_dump($result);
         return $result ;
    }

    public function getCategoryMenu()
    {
        return $this->createQueryBuilder('a')
            ->innerJoin( 'a.category','c')
            ->select('c.id, c.label, count(a.id) as count')
            ->orderBy('c.label', 'ASC')
            ->groupBy('c.id, c.label')
            ->getQuery()
            ->getResult()            ;
    }

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
