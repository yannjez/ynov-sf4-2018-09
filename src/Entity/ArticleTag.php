<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

// test

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleTagRepository")
 */
class ArticleTag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function __toString() {
        return $this->label;
    }
}
