<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009065201 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_23A0E6612469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__article AS SELECT id, category_id, title, content, slug FROM article');
        $this->addSql('DROP TABLE article');
        $this->addSql('CREATE TABLE article (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, content CLOB DEFAULT NULL COLLATE BINARY, slug VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES article_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO article (id, category_id, title, content, slug) SELECT id, category_id, title, content, slug FROM __temp__article');
        $this->addSql('DROP TABLE __temp__article');
        $this->addSql('CREATE INDEX IDX_23A0E6612469DE2 ON article (category_id)');
        $this->addSql('DROP INDEX IDX_B15FE9ED015F491');
        $this->addSql('DROP INDEX IDX_B15FE9E7294869C');
        $this->addSql('CREATE TEMPORARY TABLE __temp__article_article_tag AS SELECT article_id, article_tag_id FROM article_article_tag');
        $this->addSql('DROP TABLE article_article_tag');
        $this->addSql('CREATE TABLE article_article_tag (article_id INTEGER NOT NULL, article_tag_id INTEGER NOT NULL, PRIMARY KEY(article_id, article_tag_id), CONSTRAINT FK_B15FE9E7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_B15FE9ED015F491 FOREIGN KEY (article_tag_id) REFERENCES article_tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO article_article_tag (article_id, article_tag_id) SELECT article_id, article_tag_id FROM __temp__article_article_tag');
        $this->addSql('DROP TABLE __temp__article_article_tag');
        $this->addSql('CREATE INDEX IDX_B15FE9ED015F491 ON article_article_tag (article_tag_id)');
        $this->addSql('CREATE INDEX IDX_B15FE9E7294869C ON article_article_tag (article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_23A0E6612469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__article AS SELECT id, category_id, title, slug, content FROM article');
        $this->addSql('DROP TABLE article');
        $this->addSql('CREATE TABLE article (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, content CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO article (id, category_id, title, slug, content) SELECT id, category_id, title, slug, content FROM __temp__article');
        $this->addSql('DROP TABLE __temp__article');
        $this->addSql('CREATE INDEX IDX_23A0E6612469DE2 ON article (category_id)');
        $this->addSql('DROP INDEX IDX_B15FE9E7294869C');
        $this->addSql('DROP INDEX IDX_B15FE9ED015F491');
        $this->addSql('CREATE TEMPORARY TABLE __temp__article_article_tag AS SELECT article_id, article_tag_id FROM article_article_tag');
        $this->addSql('DROP TABLE article_article_tag');
        $this->addSql('CREATE TABLE article_article_tag (article_id INTEGER NOT NULL, article_tag_id INTEGER NOT NULL, PRIMARY KEY(article_id, article_tag_id))');
        $this->addSql('INSERT INTO article_article_tag (article_id, article_tag_id) SELECT article_id, article_tag_id FROM __temp__article_article_tag');
        $this->addSql('DROP TABLE __temp__article_article_tag');
        $this->addSql('CREATE INDEX IDX_B15FE9E7294869C ON article_article_tag (article_id)');
        $this->addSql('CREATE INDEX IDX_B15FE9ED015F491 ON article_article_tag (article_tag_id)');
    }
}
