<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\ArticleTag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class App extends Fixture
{
    public function load(ObjectManager $manager)
    {


        $faker = \Faker\Factory::create('fr_FR');
        // $product = new Product();
        // $manager->persist($product);


        $categories = [];
        $tags= [];

        for($i=1; $i<=3;$i++){
            $item = new ArticleTag();
            $item->setLabel($faker->century );
            $tags[] = $item;
            $manager->persist($item);
        }

        for($i=1; $i<=3;$i++){
            $item = new ArticleCategory();
            $item->setLabel($faker->safeColorName );
            $categories[] = $item;
            $manager->persist($item);
        }

        for($i=1; $i<=10;$i++){
            $item = new Article();
            $item->setTitle($faker->title );
            $item->setContent($faker->paragraph());
            $item->setSlug('article-'.$i);
            $categorieIndex = $i%3 ;
            $item->setCategory($categories[$categorieIndex]);
            $manager->persist($item);
            shuffle($tags);
            switch ($i%3){
                case 1 :
                    $item->setTags([$tags[0]]);
                    break;
                case 2 :
                    $item->setTags([$tags[0],$tags{1}]);
                    break;
            }
        }
        $manager->flush();
    }
}
