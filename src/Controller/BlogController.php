<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_index",  requirements={"page"="\d+"},  defaults={"page"="1"})
     */
    public function index($page)
    {
        // dans la route on force le paramètre à être numérique, et s'il n'est pas précisé on lui affecte la valeur "1"
        $articles = $this->getData($page);
        $viewData = ['articles'=>$articles];
        $viewData['page']=$page;
        return $this->render('blog/index.html.twig', $viewData);
    }

    /**
     * @Route("/blog/{slug}", name="blog_detail",methods={"GET"})
     */
    public function detail($slug,RequestStack $requestStack)
    {

        // on récupère l'identifiant de l'article et on recré l'article
        $id = str_replace( 'article_','',$slug);
        $article=  ['title' => 'Article ' .$id, 'slug'=>$slug];
        $viewData = ['article'=>$article];

        // on récupère les paramètres dans l'URL /blog/article_6?toto=56&tata=vert
        $request = $requestStack->getCurrentRequest();
        $request->query->get('toto');
        $request->query->get('tata');
        $allQuery = $request->query->all();
        $viewData['allQuery'] = $allQuery;
        return $this->render('blog/detail.html.twig', $viewData);
    }

    /**
     * @Route("/blog/{slug}", name="blog_like",methods={"POST"})
     */
    public function like($slug,UrlGeneratorInterface $urlGenerator,RequestStack $requestStack)
    {
        // cette méthode n'est appelé que par les post de formulaire

        // on demande au contenair d'injection de dépendance les services suivants  UrlGeneratorInterface et RequestStack


       // UrlGeneratorInterface permet de générer les URL en s'appuyant sur le nom technique de la route
        $url = $urlGenerator->generate('blog_detail',['slug'=>$slug]);

        //RequestStack  contient dans 99% des cas une seul requete qui celle fourni par le navigateur
        $request = $requestStack->getCurrentRequest();

        //  méthode pour récupérer les données dans les formulaires
        //      <input type="hidden" name="titi" value="58">
        //        <input type="hidden" name="toto" value="vert">
        $request->request->get('toto');
        $request->request->get('titi');
        $request->request->all();

        //  méthode pour récupérer les données dans les formaulaires ou dans le paramètre d'URL
        $request->get('titi');
        //$viewData['allQuery'] = $allRequest;

        // on provioque une navigation vers la méthode GET
        return new RedirectResponse($url);
    }

    public function getData($page){

        // on crée des données fictives
        $start = ($page - 1)  *  5 ;
       // var_dump($page , $start);
        $articles = [];
        for($i=$start ; $i < $start + 5 ; $i++){
            $articles[] = ['title' => 'Article ' .$i, 'slug'=>'article_'.$i ];
        }
        return $articles;
    }
}
