<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {

        $viewData = [];
        $viewData['name']= 'yann';
        $viewData['civil']= 'Mr';
        $viewData['details']=['info'=>'info','summary'=>'summary'];
        $ex = new \Exception("test");


        $viewData['products']= [
            ['name'=>'p1','price'=>120],
            ['name'=>'p2','price'=>250],
            ['name'=>'p3','price'=>1200],
            ['name'=>'p4','price'=>1200]
,        ];

        $viewData['ex']=$ex;
        $viewData['comment'] = '<script> alert("toto") </script>';
        $viewData['tata'] = null;
        return $this->render('test/index.html.twig', $viewData);
    }

    /**
     * @Route("/", name="test_index2")
     */
    public function index2() {
        $viewData= [];
        $viewData['products']= [
            ['name'=>'p1','price'=>120],
            ['name'=>'p2','price'=>250],
            ['name'=>'p3','price'=>1200],
            ['name'=>'p4','price'=>1200]
            ,        ];
        return $this->render('test/indexwithparent.html.twig', $viewData);
    }

}
