<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BlogDbController extends AbstractController
{

    private $articleRepo;


    public function __construct(ArticleRepository $articleRepo) {
        $this->articleRepo = $articleRepo;
    }


    /**
     * @Route("/blogdb/{categ}", name="blog_db_index", defaults={"categ":"0"}, requirements={"categ"="\d+"})
     */
    public function index($categ) {
        $viewData = [];

        if (empty($categ)) {
            $viewData['articles'] = $this->articleRepo->findAll();
        } else {
            $viewData['articles'] = $this->articleRepo->findBy(['category' => $categ]);
        }
        $viewData['categories'] = $this->articleRepo->getCategoryMenu();
        // var_dump($viewData['categories']);
        return $this->render('blog_db/index.html.twig', $viewData);
    }

    /**
     * @Route("/blogdb/{slug}", name="blog_db_detail")
     */
    public function detail($slug) {
        $viewData = [];
        $viewData['article'] = $this->articleRepo->findOneBy(['slug' => $slug]);
        $viewData['articleSlug'] = $this->articleRepo->findBySlug($slug);
        $viewData['slug'] = $slug;
        return $this->render('blog_db/detail.html.twig', $viewData);
    }

    /**
     * @Route("/blogdb/edit/{slug}", name="blog_db_edit")
     */
    public function edit($slug,FormFactoryInterface $formFactory,Request $request,UrlGeneratorInterface $urlGenerator) {
        $viewData = [];

        $article = $this->articleRepo->findOneBy(['slug' => $slug]);
        $viewData['article'] =$article;
        $builder = $formFactory->createBuilder(ArticleType::class,$article);
        $builder->add('submit', SubmitType::class, [
            'label' => 'Sauver',
            'attr' => ['class' => 'btn btn-default pull-right']]);

        /* supprimer un champ */
       //  $builder->remove('title');
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if($form->isValid()){
                $this->getDoctrine()->getManagerForClass(Article::class)->flush();
                $url = $urlGenerator->generate('blog_db_detail',['slug'=>$slug]);
                return new RedirectResponse($url);
            }
        }

        $viewData['form']= $form->createView();
       // $viewData['articleSlug'] = $this->articleRepo->findBySlug($slug);
        return $this->render('blog_db/edit.html.twig', $viewData);
    }
}
