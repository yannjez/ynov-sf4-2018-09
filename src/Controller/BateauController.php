<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BateauController extends AbstractController
{
    /**
     * @Route("/home", name="bateau_index")
     */
    public function index() {

        $viewData = $this->getData();

        return $this->render('bateau/index.html.twig', $viewData);
    }

    /**
     * @Route("/bateau/{id}", name="bateau_bateau")
     */
    public function bateau($id) {

        //$viewData = $this->getData();
        $item = $this->getDataByid('boat',$id);
        $viewData = ['boat'=>$item];
        return $this->render('bateau/bateau.html.twig', $viewData);
    }

    /**
     * @Route("/skipper/{id}", name="bateau_skipper",methods={"GET"})
     */
    public function skipper($id) {

        //$viewData = $this->getData();
        $item = $this->getDataByid('skipper',$id);
        $viewData = ['skipper'=>$item];
        return $this->render('bateau/skipper.html.twig', $viewData);
    }
    /**
     * @Route("/skipper/{id}", name="bateau_support",methods={"POST"})
     */
    public function support($id,UrlGeneratorInterface  $urlGenerator) {
        $url = $urlGenerator->generate('bateau_skipper',['id'=>$id]);
        return new RedirectResponse($url);
    }

    private function getDataByid($type, $id) {
        if ($type == 'skipper') {
            return ['id' => $id, 'firstname' => 'skipper ' . $id, 'lastname' => 'jhon' . $id];
        } else {
            $name = $type === 'boat' ? 'bateau ' : 'course';
            return ['id' => $id, 'name' => $name . ' ' . $id];
        }
    }

    private function getData() {

        $boats = [];
        for ($i = 1; $i <= 7; $i++) {
            $boats[] = ['id' => $i, 'name' => 'bateau ' . $i];
        }

        $skippers = [];
        for ($i = 1; $i <= 4; $i++) {
            $skippers[] = ['id' => $i, 'firstname' => 'skipper ' . $i, 'lastname' => 'jhon' . $i];
        }
        $races = [];
        for ($i = 1; $i <= 3; $i++) {
            $races[] = ['id' => $i, 'name' => 'course ' . $i];
        }
        return ['boats' => $boats, 'skippers' => $skippers, 'races' => $races];

    }


}
