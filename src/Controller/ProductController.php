<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product_index")
     */
    public function index()
    {
        $product = $this->getData();
        $viewData = ['product'=>$product];
        return $this->render('product/index.html.twig',$viewData);
    }

    /**
     * @Route("/product2", name="product2")
     */
    public function index2()
    {
        $product = $this->getData();
        $viewData = ['product'=>$product];
        return $this->render('product/index.html.twig',$viewData);
    }

    public function getData(){
        $product = [
            'originalUrl'=>'https://www.michenaud.com/p11185/martin-lxk2-little-martin-koa.php'
            , 'image'=>'https://www.michenaud.com/upDATA/p/11500/11185/img1427702236-FtwYJ/500medium.jpg'
            , 'title'=>'MARTIN LXK2'
            ,'size'=>[100,100]
            ,'Technic'=>'Mixte'
            ,'style '=>'Figuratif'
            ,'info'=>'Guitare folk MARTIN LXK2 Little Martin koa '
            ,'price'=>459
            ,'brand'=>'martin&co'
            ,'brandLogo'=>'https://www.michenaud.com/upDATA/imgMarque/159.gif'
            ,'summary'=>'La Série X Martin porte depuis quelques années son attention sur de nouveaux matériaux de fabrication. Cette gamme est issue d\'une combinaison entre bois traditionnels et fibres compressées, permettant de réaliser des guitares à prix abordables, capables d\'offrir le confort de jeu et le "son Martin".'
            ,'warranty'=>'12Y'
            ,'details'=>[
                'Table Koa HPL.'
                ,'Barrage style "1".'
                ,'Dos et éclisses HPL Koa.'
                ,'Manche stratabond naturel.'
                ,'Touche et chevalet morado.'
                ,'Plaque type écaille de tortue.'
                ,'Mécaniques chrome.'
                ,'Livrée en housse.'
                ,'Finition satinée.'
            ]
            , 'stock'=>['internet'=>true,'nantes'=>false]
        ];
        return $product ;
    }
}
